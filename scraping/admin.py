from django.contrib import admin
from .models import Car, Photo


class PhotoInline(admin.TabularInline):
    model = Photo
    extra = 1
    readonly_fields = ['image_tag']
    fields = ['photo', 'image_tag']


class CarAdmin(admin.ModelAdmin):
    inlines = [PhotoInline]


admin.site.register(Car, CarAdmin)
