from django.db import models
from django.utils.html import format_html


class Car(models.Model):
    name = models.CharField(max_length=50)
    legacy = models.CharField(max_length=50)
    date = models.DateField(auto_now=True)
    body = models.CharField(max_length=50)
    color = models.CharField(max_length=20)
    state = models.CharField(max_length=50)
    steering_wheel = models.CharField(max_length=10)
    year = models.CharField(max_length=100, null=True)

    def __str__(self):
        return self.name


class Photo(models.Model):
    photo = models.ImageField(upload_to='media/')
    car = models.ForeignKey(Car, on_delete=models.CASCADE)

    def image_tag(self):
        return format_html('<img src="{}" width="50" height="50" />', self.photo.url)

    image_tag.short_description = 'Изображение'
