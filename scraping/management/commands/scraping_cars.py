import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from fake_useragent import UserAgent
from bs4 import BeautifulSoup
from django.core.management.base import BaseCommand
from scraping.models import Car, Photo
from requests import request
import uuid


class Command(BaseCommand):
    help = 'Скрапинг данных о машинах'

    def handle(self, *args, **kwargs):
        options = Options()
        ua = UserAgent()
        user_agent_random = ua.random
        options.add_argument(f'user-agent={user_agent_random}')

        driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()), options=options)
        url = 'https://auto.ru/cars/mercedes/all/'

        try:
            page = 1
            while True:
                if not self.parsing_actions(driver, url, page):
                    break
                page += 1
        except Exception as ex:
            self.stdout.write(self.style.ERROR(str(ex)))
            input("Пожалуйста, введите капчу вручную и нажмите Enter...")
            try:
                page += 1
                self.parsing_actions(driver, url, page)
            except Exception as ex2:
                self.stdout.write(self.style.ERROR(str(ex2)))
        finally:
            driver.close()
            driver.quit()

    def parsing_actions(self, driver, url, page):
        driver.get(f'{url}/?page={page}')

        WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, 'confirm-button'))
        )

        confirmation_button = driver.find_element(by=By.ID, value='confirm-button')
        confirmation_button.click()

        car_summaries = WebDriverWait(driver, 10).until(
            EC.presence_of_all_elements_located((By.CLASS_NAME, 'ListingItem__summary'))
        )

        links_to_visit = [
            summary.find_element(by=By.TAG_NAME, value='a').get_attribute('href') for summary in car_summaries
        ]

        for link in links_to_visit:
            driver.get(link)
            time.sleep(3)
            soup = BeautifulSoup(driver.page_source, 'lxml')
            title = soup.select_one('.CardHead__title')
            year = soup.select_one('.CardInfoRow_year .Link')
            body_type = soup.select_one('.CardInfoRow_bodytype .Link')
            color = soup.select_one('.CardInfoRow_color .Link')
            legacy = soup.select_one('.CardInfoRow_superGen .Link')
            wheel = soup.select('.CardInfoRow_wheel .CardInfoRow__cell')
            state = soup.select('.CardInfoRow_state .CardInfoRow__cell')

            if year is not None:

                car = Car(
                    name=title.text,
                    year=year.text,
                    body=body_type.text,
                    color=color.text,
                    legacy=legacy.text,
                    state=state[1].text,
                    steering_wheel=wheel[1].text

                )
                car.save()
                first_image = driver.find_element(by=By.CLASS_NAME, value='ImageGalleryDesktop__image')
                first_image.click()
                WebDriverWait(driver, 10).until(
                    EC.presence_of_all_elements_located((By.CLASS_NAME, 'ImageGalleryFullscreenVertical__image'))
                )

                images = driver.find_elements(by=By.CLASS_NAME, value='ImageGalleryFullscreenVertical__image')

                for img in images:
                    src = img.get_attribute('src')
                    if not src.startswith('https:'):
                        src = 'https:' + src
                    req_img = request('get', src)

                    filename = str(uuid.uuid4())
                    with open(f'media/{filename}.png', 'wb') as image_file:
                        image_file.write(req_img.content)
                    car_image = Photo(car=car)
                    car_image.photo = f'{filename}.png'
                    car_image.save()
        next_page_button = driver.find_element(by=By.CLASS_NAME, value='Button__text')
        time.sleep(3)
        return next_page_button.is_enabled()

